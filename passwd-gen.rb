#!/usr/bin/env ruby
#
# Password Generator
#
require 'securerandom'
require 'io/console'
require 'base64'

def gen_pswd(pwLen)
  pw=''
  for i in 1..pwLen do
      pw += (65..122).to_a.sample.chr
   end
   return pw
end

# def gen_pswd(pwlen)
#     pwlen = pwlen.to_i
#     print "\n\t#{SecureRandom.base64(pwlen)[0..pwlen - 1]}"
# end

############# Main ##################

print "\nNumber of passwords to generate: "
times = gets.chomp.to_i

begin
  print "\tPassword Lenth: "
  pwlen = gets.chomp.to_i
end until pwlen != ''
puts "generating #{times} passwords"
(1..times).each { |i|
    puts(gen_pswd(pwlen))

}
print "\n"
