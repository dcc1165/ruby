#!/usr/bin/env ruby
require 'rubygems'
require 'gosu'

class Ball
  attr_reader :x, :y, :w, :h
  def initialize(the_window)
    @x = 200
    @y = 400
    @vx = 5
    @vy = 5
    @w = 20
    @h = 20
    @image = Gosu::Image.new(the_window, "ball.png",false)
  end

  def reverse
    @vx = -1 * @vx
  end

  def draw
    @image.draw(@x, @y, 1)
  end

  def move
    @x = @x + @vx
    @y = @y + @vy
    if @y > 580
      @vy = -5
    end
    if @x > 780
      @vx = -5
    end
    if @y < 0
      @vy = 5
    end
    if @x < 0
      @vx = 5
    end
  end
end

class Paddle
  attr_reader :x, :y, :w, :h
  def initialize(the_window, xpos)
    @x = xpos
    @y = 300
    @w = 20
    @h = 60
    @image = Gosu::Image.new(the_window, "paddle.png",false)
  end
  def move_up
    if @y > 0
      @y = @y - 5
    end
  end

  def move_down
    if @y < 540
      @y = @y + 5
    end
  end

  def draw
    @image.draw(@x, @y, 1)
 end

end


class GameWindow < Gosu::Window
  def initialize
    super 800,600,false
    self.caption = "Pong"
    @ball = Ball.new(self)
    @left_paddle = Paddle.new(self,20)
    @right_paddle = Paddle.new(self,760)
  end

  def are_touching?(obj1, obj2)
    obj1.x > obj2.x - obj1.w and obj1.x < obj2.x + obj2.w and obj1.y > obj2.y - obj1.h and obj1.y < obj2.y + obj2.h
  end

  def update
    @ball.move
    if button_down?(Gosu::KbW)
      @left_paddle.move_up
    end
    if button_down?(Gosu::KbS)
      @left_paddle.move_down
    end
    if button_down?(Gosu::KbUp)
      @right_paddle.move_up
    end
    if button_down?(Gosu::KbDown)
      @right_paddle.move_down
    end
    if are_touching?(@ball, @left_paddle)
      @ball.reverse
    end
    if are_touching?(@ball, @right_paddle)
      @ball.reverse
    end
  end

  def draw

  end
end

window = GameWindow.new
window.show
