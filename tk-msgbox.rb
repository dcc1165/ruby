#!/usr/bin/env ruby

require 'tk'

root = TkRoot.new
root.title = "Window"

msgBox = Tk.messageBox(
  'type'    => "ok",  
  'icon'    => "info", 
  'title'   => "This is the title",
  'message' => "This is the message"
)
Tk.mainloop
