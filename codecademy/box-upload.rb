require 'rest-client'
require 'json'

your_folder_id = "4153776541"

your_filename = "TestFileFromAPI" + ".txt"

f = File.open(your_filename, "w")

# Write a string to the file here–don't forget to close the file!
f.write("This is a test of the emergency broadcast system\n")
f.close()
response = RestClient.post("https://upload.box.com/api/2.0/files/content",
    {
    	# We need to read the file again to send it to Box
        :myFile => File.new(your_filename, 'rb'),
        # This tells Box where to put the file
        :parent_id => your_folder_id
    },
    :authorization => "Bearer " << "AKq3WW8HuVlW0LI0shh2IryDta6nyzwa"
)
puts response
# puts JSON.parse(response.body)["entries"][0]["name"]
