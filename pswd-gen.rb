require 'securerandom'
require 'base64'
require 'io/console'

def gen_pswd
  begin
    begin
      print "Password length: "
      length = gets.chomp
    end until length != ''
    length = length.to_i
    begin
       print "How many: "
       count = gets.chomp.to_i
    end until count > 0
    for i in 1..count do
       print "\n\t #{SecureRandom.base64(length)[0..length - 1]}"
    end
    yorn = yorn("\n\nGenerate another?",'n')
    puts
  end while yorn
end

def yorn(prompt,default)
  default.upcase!
  yn='y/n'
  yn=yn.sub(yn[default.downcase],default)
  begin
     print prompt + " [#{yn}]: "
     ans = STDIN.getch.downcase
     return false if ans.ord == 3
     ans = default.downcase if ans.ord == 13 or ans == 10 or ans == ''
  end until ans == 'y' or ans == 'n'
  yorn = ans == "\n" ? default.downcase : ans
  yorn == 'y' ? true : false
end

gen_pswd
