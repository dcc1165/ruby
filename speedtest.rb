require 'speedtest'

test = Speedtest::Test.new(
  download_runs: 4,
    upload_runs: 4,
    ping_runs: 4,
    download_sizes: [750, 1500],
    upload_sizes: [1000, 40000],
    debug: false
)

results = test.run

puts results
