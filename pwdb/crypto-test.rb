#!/usr/bin/env ruby
#
# Poor-man's password repo
#

require 'digest'
require 'openssl'
require 'yaml'
require 'io/console'

$pswd=""
$iv="a3417c84-46c0-4929-a84b-a72d29b8444b"

class PWinfo
   attr_reader :entryName, :loginID, :password

   def initialize(entryName, loginID, password)
       @entryName=entryName
       @login_id=loginID
       @password=password
   end

   def show
       puts("Entry Name: #{@entryName}\nLogin ID: #{@login_id}\nPassword: #{@password}\n")
   end
end

def get_password(prompt="Password: ")
    print prompt
    STDIN.noecho(&:gets).chomp
end

# def encrypt(data, key)
def encrypt(data)
    puts "pswd: #{$pswd}"
    key = Digest::MD5.digest($pswd) if($pswd.kind_of?(String) && 16 != $pswd.bytesize)
    puts "Key: #{key}"
    aes = OpenSSL::Cipher.new('AES-128-CBC')
    aes.encrypt
    aes.key = key
    aes.iv = $iv
    return aes.update(data) + aes.final
end

# def decrypt(data,key)
def decrypt(data)
    puts "pswd: #{$pswd}"
    key = Digest::MD5.digest($pswd) if($pswd.kind_of?(String) && 16 != $pswd.bytesize)
    puts "Key= #{key}"
    aes = OpenSSL::Cipher.new('AES-128-CBC')
    aes.decrypt
    aes.key = key
    aes.iv = $iv
    return aes.update(data) + aes.final
end

def listPW
end

def findPW(args)
end

def get_password(prompt="Password: ")
    print prompt
    STDIN.noecho(&:gets).chomp
end

$pswd = get_password

testStr="This is a test of the emergency broadcast system"
#key = Digest::MD5.digest($pswd) if($pswd.kind_of?(String) && 16 != $pswd.bytesize)
# encStr=encrypt(testStr, key)
encStr=encrypt(testStr)
puts "Encrypted: #{encStr}"

# decStr=decrypt(encStr, key)
decStr=decrypt(encStr)
puts "Decrypted: #{decStr}"

# pwList=[]
# count=10

# (0..count).each { |i| 
#       pwList[i] = PWinfo.new( "server" + i.to_s,
#                               "root" + i.to_s,
#                               "pswd" + i.to_s )
# }

# pwList.each { |i| i.show }
# print "Hit Enter:"

# xx=gets
# pwList_y=pwList.to_yaml
# puts pwList_y

