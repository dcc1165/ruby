#!/usr/bin/env ruby
#
# Poor-man's password repo
#

require 'yaml'
require 'io/console'
require './crypto'
require 'base64'

$pswd=""
$fname = "pw-store.pmgr"
$iv="a3417c84-46c0-4929-a84b-a72d29b8444b"

class Entryinfo
  attr_reader :entryName,:loginID,:password,:description

  def initialize(entryName, loginID, password)
    @entryName=entryName
    @login_id=loginID
    @password=password
  end

  def show(with_password = false)
    puts("Entry Name: #{@entryName}\nLogin ID: #{@login_id}")
    puts("\nPassword: #{@password}\n") if with_password == true
  end
end

def get_password(prompt="Password: ")
  print prompt
  STDIN.noecho(&:gets).chomp
end

def get_filename
    f_name = $fname
    print "Enter Filename [#{f_name}]:"
    xx = gets.chomp
    f_name=xx if xx.size > 0
    return f_name
end

def check_file(fname)
  print "#{fname}: Does not exist.  "
  begin 
    print "Create #{fname} [y]? "
    yorn=gets.chomp.upcase
    yorn = "Y" if yorn.size == 0
  end until yorn == "Y" or yorn == "N"

  if yorn == "Y"
    $pswd=get_password("Enter password for new file '#{fname}': ")
    # Create the new file here; write encrypted/base64 encoded password
    return true
  else
    return false
  end
end

def get_password(prompt="Password: ")
  print prompt
  STDIN.noecho(&:gets).chomp
end

############# Main ##################
fname=get_filename if ARGV.size == 0
exit if !check_file(fname)

puts "Filename: #{fname}\n"
$pswd = get_password

entryList=[]
count=10

(0..count).each { |i| 
   entryList[i] = Entryinfo.new( "server" + i.to_s,
                           "root" + i.to_s,
                           "pswd" + i.to_s )
}

# listEntries(entryList)

# Encrypt password file
#entryList_y=Base64.encode64(Crypto.encrypt(entryList.to_yaml))
entryList_y=entryList.to_yaml
fptr=File.open(fname,"w")
#fptr.write(Base64.encode64(Crypto.encrypt($pswd)))
fptr.write($pswd)
fptr.write(entryList_y)
fptr.close
# print "Hit Enter:"
# gets

# Decrypt password file
# File.write( fname, Crypto.decrypt(Base64.decode64(entryList_y)))

