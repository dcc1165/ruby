#!/usr/bin/env ruby
#
# Utility to dump a password store.  Incredibly insecure, by design.
# Currently the output format is YAML, but other formats are coming
# i.e. CSV, character-delimited (tab, pipe, etc) and any other ones
# I'm not too lazy to implement :)
#
require 'yaml'
require 'securerandom'
require 'io/console'
require './crypto'
require 'base64'

trap 'SIGINT' do
  puts "\nExitting..."
  exit 0
end

$pswd=''
$fname='pw-store.pmgr'

$iv='a3417c84-46c0-4929-a84b-a72d29b8444b'
$fmt="%35s\t%25s\t%25s\t%20s"

class Entryinfo
  attr_reader :entry_name,:login_id,:password,:description

  def initialize(entry_name, login_id, password, description)
    @entry_name = entry_name
    @login_id = login_id
    @password = password
    @description = description
  end

  def show(with_password = false)
    if with_password
       puts sprintf($fmt,@entry_name, @description, @login_id, @password)
    else
       puts sprintf($fmt,@entry_name, @decription, @login_id, "<hidden>")
    end
  end
  
end # EntryInfo

def get_password(prompt='Password: ')
  print prompt
  STDIN.noecho(&:gets).chomp
end

def get_filename
  fname = $fname
  print "Enter Filename [#{fname}]: "
  xx = gets.chomp
  fname = xx if xx.size > 0
  fname = fname.include?('.') ? fname : fname + '.pmgr'
#  $fname = fname
  return fname
end

def check_file(fname)
  success = false
  begin
    if File.exists?(fname)
      success = true
    else
      print "'#{fname}' Does not exist.  "
      puts
      fname = get_filename
    end
  end until success
  $fname = fname
  puts "$fname = #{$fname}"

  success=false
  begin
    $pswd = get_password
    success = load_file(fname, $pswd)
    puts "Error loading file." if !success
  end until success
end

def load_file(fname, password)
  File.copy_stream(fname,fname + "-save")
  file=File.open(fname, 'r')
  # read the first line (password), die if wrong
  begin
    pswd = Crypto.decrypt(Base64.decode64(file.readline.chomp))
  rescue
    pswd = 'BAD'
  end

  if pswd != password
    puts "\nPassword is incorrect"
    return false
  else
    enc_data = file.read
  end

  if enc_data != ''
    file_data = Crypto.decrypt(Base64.decode64(enc_data))
    $entry_list = YAML::load( file_data )
  end
  true
end

def cls
  puts "\e[H\e[2J"
end

############# Main ##################
$entry_list=[]
$fname=get_filename if ARGV.size == 0
check_file($fname)
begin
  print "What format?\n"
  puts "[1] Pipe"
  puts "[2] CSV"
  puts "[3] YAML"
  print "\n--> "
  outFormat=gets.chomp.to_i
  success = true if [1,2, 3].include?(outFormat)
end until success

case outFormat
  when 1
    sep="|"
    ext=".txt"
  when 2
    sep=","
    ext=".csv"
  when 3
    ext=".yaml"
end   

outFileName=$fname.split('.')[0] + ext

print "Output file [#{outFileName}]: "
fname=((gets.chomp).split('.')[0]) 
outFileName=fname.split('.')[0] + ext if fname != nil

# outFileName += ext if outFileName.split('.')[1] == nil
if ext == ".yaml"
  puts $entry_list.to_yaml
  exit
end

fmt="%s"
(1..4).each { |i| fmt +=sep + "%s" }
fmt +="\n"

outFile=File.open(outFileName, 'w')
id=1
$entry_list.each do |entry|
#   puts sprintf(fmt, id, entry.entry_name, entry.login_id, entry.password, entry.description)
   outFile.write(sprintf(fmt, id, entry.entry_name, entry.login_id, entry.password, entry.description))
   id += 1
end

