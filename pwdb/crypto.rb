module Crypto
  require 'digest'
  require 'openssl'
  require 'yaml'
  require 'io/console'

  def Crypto.encrypt(data)
    key = Digest::MD5.digest($pswd) if($pswd.kind_of?(String) && 16 != $pswd.bytesize)
    aes = OpenSSL::Cipher.new('AES-128-CBC')
    aes.encrypt
    aes.key = key
    aes.iv = $iv
    return aes.update(data) + aes.final
  end

  def Crypto.decrypt(data)
    key = Digest::MD5.digest($pswd) if($pswd.kind_of?(String) && 16 != $pswd.bytesize)
    aes = OpenSSL::Cipher.new('AES-128-CBC')
    aes.decrypt
    aes.key = key
    aes.iv = $iv
    return aes.update(data) + aes.final
  end
end
