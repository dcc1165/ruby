PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
    CREATE TABLE passdb (id int primary key not null,
        name text(128) not null,
        login_id text(128) not null,
        password text(128) not null,
        description text(256) not null);
commit;
