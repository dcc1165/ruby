#!/usr/bin/env ruby
#
# Poor-man's password repo
#
require 'yaml'
require 'securerandom'
require 'io/console'
require './crypto'
require 'base64'

trap 'SIGINT' do
  puts "\nExitting..."
  exit 0
end

$pswd=''
$fname='pw-store.pmgr'

$iv='a3417c84-46c0-4929-a84b-a72d29b8444b'

# Name Description LoginID password
$fmt="    %-25s\t%-35s\t%-20s\t%-20s"

menu_list={ :A => ['add_entry($entry_list)','Add a NEW Password Entry'],
            :D => ['del_entry($entry_list)','Delete a Password Entry'],
            :M => ['mod_entry($entry_list)','Modify a password Entry'],
            :S => ['show_entry($entry_list)','Search/Display a password entry'],
            :L => ['list_entries($entry_list)','List Password Database'],
            :P => ['change_pswd($pswd,$entry_list)','Change DB Password'],
            :G => ['gen_pswd','Generate Random Password String']
}

class Entryinfo
  attr_reader :entry_name,:login_id,:password,:description

  def initialize(entry_name, description, login_id, password)
    @entry_name = entry_name
    @login_id = login_id
    @password = password
    @description = description
  end

  def show(with_password = false)
    if with_password
       puts sprintf($fmt, @entry_name, @description, @login_id, @password)
    else
       puts sprintf($fmt, @entry_name, @description, @login_id, "<hidden>")
       # puts sprintf("%35s\t%25s",@entry_name, @login_id)
    end
  end
  
# Add the parameter to the pasteboard.
  def pbcopy()
    str = @password.to_s
    IO.popen('pbcopy', 'w') { |f| f << str }
    str
  end

end

def get_password(prompt='Password: ')
  print prompt
  STDIN.noecho(&:gets).chomp
end

def get_filename
  fname = $fname
  print "Enter Filename [#{fname}]: "
  xx = gets.chomp
  fname = xx if xx.size > 0
  fname = fname.include?('.') ? fname : fname + '.pmgr'
#  return fname
end

# Entry manipulation methods
def add_entry(pw_list)
  print "\tEntry Name: "
  entry_name=gets.chomp
  return if entry_name == ''
  print "\tDescription: "
  description = gets.chomp
  print "\tLogin ID: "
  login_id = gets.chomp
  pswd = get_password("\tPassword for '#{login_id}': ")
  verify_pswd = get_password("\n\tPassword Verification: ")
  puts
  if pswd != verify_pswd
    puts "\n\tPasswords don't match.  Entry NOT saved."
  else
    puts "\n\tSaving Entry..."
    pw_list << Entryinfo.new(entry_name,description,login_id,pswd)
    write_file(pw_list)
    puts "\n\tEntry saved."
  end
end

def del_entry(pw_list)
  srch = search_prompt
  return if srch == ''
  new_list = []
  show_pass = yorn("\tShow passwords?","n")
  puts "\n"
  puts sprintf($fmt,"Entry Name", "Description", "Login ID", "Password")
  pw_list.each { |pw|
    if pw.entry_name.downcase.include? srch
      pw.show(show_pass)
      if !yorn("\tDelete? ", "n")
        new_list << pw
      else
         print "\n\tDeleted.\n"
      end
    else
      new_list << pw
    end
#    puts("\nnewList (#{newList.length}): ")
#    newList.each { |entry| puts entry.entry_name }
  }
  $entry_list = new_list.clone # if newList.length > 0
  write_file($entry_list)
end

def mod_entry(pw_list)
  print "This is mod_entry\n"
end

def list_entries(pw_list)
  if pw_list.length == 0
    puts "\tPassword List is Empty!\n"
  else
     puts sprintf($fmt,"Entry Name", "Description", "Login ID", "Password")
     pw_list.each { |entry| entry.show }
  end
end

def change_pswd(pswd, pw_list)
  old_pswd = get_password("\t  Old Password: ")
  if old_pswd != pswd
    puts "\n\n\tError: Password incorrect."
  else
    new_pswd = get_password( "\n\t  New Password: ")
    verify_pswd = get_password("\nNew Password Again: ")

    if new_pswd != verify_pswd
      puts "\n\tPasswords don't match"
    else
      puts
      $pswd = new_pswd
      write_file(pw_list)
      puts "\n\tPassword changed."
    end
  end
end

def gen_pswd
  begin
    begin
      print "\tPassword length: "
      length = gets.chomp
    end until length != ''
    length = length.to_i
    print "\n\tPassword String: #{SecureRandom.base64(length)[0..length - 1]}\n"
    yorn = yorn("\n\tGenerate another?",'n')
    puts
  end while yorn
end

def search_prompt
  print "\tSearch String: "
  gets.chomp.downcase
end

def show_entry(pw_list)
  srch = search_prompt
  return if srch == ''
  show_pass = yorn("\tShow passwords?", 'n')
  puts "\n"
  puts sprintf($fmt,"Entry Name", "Description", "Login ID", "Password")

  pw_list.each { |pw|
    if pw.entry_name.downcase.include? srch
      pw.show(show_pass)
      if yorn("\n\tCopy to clipboard", 'n')
         pw.pbcopy
      end
    end
  }
end

def yorn(prompt,default)
  default.upcase!
  yn='y/n'
  yn=yn.sub(yn[default.downcase],default)
  begin
     print prompt + " [#{yn}]: "
     ans = STDIN.getch.downcase
     return false if ans.ord == 3
     ans = default.downcase if ans.ord == 13 or ans == 10 or ans == ''
     puts
  end until ans == 'y' or ans == 'n'
  yorn = ans == "\n" ? default.downcase : ans
  yorn == 'y' ? true : false
end

def pause
  print "\n\t[Press any key to continue] "
  STDIN.getch
end

def create_file(fname)
  begin
    new_pswd=get_password("\nEnter Password for '#{fname}': ")
    verify_pswd=get_password("\nEnter Password Again: ")
    puts "\n\tPasswords don't match" if verify_pswd != new_pswd
  end until new_pswd == verify_pswd
  $pswd = new_pswd
  fptr=File.open($fname, 'w')
  fptr.write(Base64.encode64(Crypto.encrypt($pswd)))
  fptr.close
end

def check_file(fname)
  if !File.exists?(fname)
    print "'#{fname}' Does not exist.  "
    if yorn("Create '#{fname}'", 'y')
      create_file(fname)
    else
      puts
      exit
    end
  else
    success=false
    begin
      $pswd = get_password
      success = load_file(fname, $pswd)
      puts "Error loading file." if !success
    end until success
  end
end

def load_file(fname, password)
  File.copy_stream(fname,fname + "-save")
  file=File.open(fname, 'r')
  # read the first line (password), die if wrong
  begin
    pswd = Crypto.decrypt(Base64.decode64(file.readline.chomp))
  rescue
    pswd = 'BAD'
  end

  if pswd != password
    puts "\nPassword is incorrect"
    return false
  else
    enc_data = file.read
  end

  if enc_data != ''
    file_data = Crypto.decrypt(Base64.decode64(enc_data))
    $entry_list = YAML::load( file_data )
  end
  true
end

# Called every time something changes (password, Add/Delete/Modify entry)
def write_file(pw_list)
  # fileData = pwList.to_yaml
  file_data =Base64.encode64(Crypto.encrypt(pw_list.to_yaml))
  fptr = File.open($fname, 'w')
  fptr.write(Base64.encode64(Crypto.encrypt($pswd)))
  fptr.write(file_data)
  fptr.close
end

def menu(choices)
  done=false
  begin
    cls
    puts "\n\n"
    choices.each { |k,v|
      puts "\t[#{k}] #{v[1]}"
    }
    puts "\t[X] Exit"
    print "\n\tEnter Choice: "

    choice = STDIN.getch.upcase
    puts

    if choice == 'X' or choice == "\u0003"
      return 'X'
    else
      if choice.ord != 127 and choice.ord != 13 and choice.ord != 8
        if choices.has_key?(choice.to_sym)
          return choices[choice.to_sym][0]
        else
          puts "\n\tInvalid choice!!" if choice != "\n"
          pause
        end
      end
    end
  end until done
end

def cls
  puts "\e[H\e[2J"
end

############# Main ##################
$entry_list=[]
$fname=get_filename if ARGV.size == 0
check_file($fname)

print "\n\n"
# method=''

while true do
  method=menu(menu_list)
  puts
  break if method == 'X'
  eval method
  pause
end
print "\tBye.\n\n"
