# Generic password store DB conversion tool

require 'yaml'
require 'securerandom'
require 'io/console'
require './crypto'
require 'base64'


$pswd=''
$fname='pwstore-test.pmgr'
$entry_list = []
$iv='a3417c84-46c0-4929-a84b-a72d29b8444b'
$fmt="%25s\t%15s\t%20s\%25s"

class Entryinfo
  attr_reader :entry_name,:login_id,:password,:description

  def initialize(entry_name, login_id, password,description)
    @entry_name = entry_name
    @login_id = login_id
    @password = password
    @description = description
  end

  def show(with_password = false)
    if with_password
       puts sprintf($fmt,@entry_name, @description, @login_id, @password)
    else
       puts sprintf($fmt,@entry_name, @description, @login_id, "<hidden>")
       # puts sprintf("%35s\t%25s",@entry_name, @login_id)
    end
  end
  
# Add the parameter to the pasteboard.
  def pbcopy()
    str = @password.to_s
    IO.popen('pbcopy', 'w') { |f| f << str }
    str
  end

end

class NewEntryinfo
  attr_reader :entry_name,:login_id,:password,:description

  def initialize(entry_name, login_id, password, description)
    @entry_name = entry_name
    @login_id = login_id
    @password = password
    @description = description
  end

  def show(with_password = false)
    if with_password
       puts sprintf($fmt,@entry_name, @description, @login_id, @password)
    else
       puts sprintf($fmt,@entry_name, @description, @login_id, "<hidden>")
       # puts sprintf("%35s\t%25s",@entry_name, @login_id)
    end
  end
  
# Add the parameter to the pasteboard.
  def pbcopy()
    str = @password.to_s
    IO.popen('pbcopy', 'w') { |f| f << str }
    str
  end

end

def load_file(fname, password)
  File.copy_stream(fname,fname + "-save")
  file=File.open(fname, 'r')
  # read the first line (password), die if wrong
  begin
   pswd = Crypto.decrypt(Base64.decode64(file.readline.chomp))
  rescue
    pswd = 'BAD'
  end

  if pswd != password
    puts "\nPassword is incorrect (#{pswd})"
    return false
  else
    enc_data = file.read
  end

  if enc_data != ''
    file_data = Crypto.decrypt(Base64.decode64(enc_data))
    $entry_list = YAML::load( file_data )
  end
  true
end

def write_file(pw_list)
  # fileData = pwList.to_yaml
  file_data =Base64.encode64(Crypto.encrypt(pw_list.to_yaml))
  fptr = File.open($fname, 'w')
  fptr.write(Base64.encode64(Crypto.encrypt($pswd)))
  fptr.write(file_data)
  fptr.close
end

def get_password(prompt='Password: ')
  print prompt
  STDIN.noecho(&:gets).chomp
end

$pswd = get_password("Enter password for pwstore-test.pmgr: ")
puts "File load error" if !load_file($fname, $pswd)
puts
new_list = []
$entry_list.each { |entry| 
  entry.show(true)
#  print "#{entry.entry_name} description: "
#  description = gets.chomp
#  new_list << NewEntryinfo.new(entry.entry_name,
#  	                           entry.login_id,
#  	                           entry.password,
#  	                           description)
#}

# new_list.each { |entry| 
  new_list << Entryinfo.new(entry.entry_name,
  	                         entry.login_id,
  	                         entry.password,
  	                         entry.description)
}
$entry_list = new_list.clone # if newList.length > 0
write_file($entry_list)
