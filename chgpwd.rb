require "io/console"
$pswd = "skynyrd"
def change_password(pswd)
  oPswd = silent_input("\n       Old Password: ")
  nPswd = silent_input( "\n      New Password: ")
  vPswd = silent_input("\nVerify New Password: ")
  
  if oPswd != pswd then
    puts "\nError: Old password is Incorrect."
  else
    if nPswd != vPswd then
      puts "\nError: Passwords don't match"
    else
      $pswd = nPswd
    end
  end
end

def silent_input(prompt)
  print prompt
  STDIN.noecho(&:gets).chomp
end

puts "Old Password: #{$pswd}"
change_password($pswd)
puts "New Password: #{$pswd}"
