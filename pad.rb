ZEROES=5
def zPad(num)
    pad="0" * ( ZEROES - num.to_s.length)
    return pad + num.to_s
end

(1..10000).each { |n| puts(zPad(n)) }
