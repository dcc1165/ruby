#!/usr/bin/env ruby

#"Ravi",
sysEng = [
	"Joel",
	"Don",
	"Forhad",
	"Mike G"
] 

startDate=Time.mktime(2016,11,1)
oneWeek=86400 * 7
nextOC=startDate

# %m/%d/%Y
(0..6).each {
  sysEng.each { |tech|
    puts "%10s %15s" % [tech, nextOC.strftime("%m/%d/%Y")]
    nextOC=nextOC + oneWeek
  }
  puts
}
puts("Done!!!!!")
