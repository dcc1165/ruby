# Remove print and append the characters to an array
# print the array in the format (xxx) xxx-xxxx (moving
# right-to-left).

def get_acro
  print "Acronym: "
  acro = gets.chomp.downcase!
end

if ARGV.size == 0
  acro=get_acro
else
  acro=ARGV[0]
end

puts acro
acro.downcase.each_char { |c| 
  case c
    when "0".."9","-","(",")"
      print "#{c}"
    when "a","b","c"
      print "2"
    when "d","e","f"
      print "3"
    when "g","h","i"
      print "4"
    when "j","k","l"
      print "5"
    when "m","n","o"
      print "6"
    when "p","q","r","s"
      print "7"
    when "t","u","v"
      print "8"
    when "w","x","y","z"
      print "9"
  end
}
puts "\n"

