require "json"

server_info_json = File.read("srvinfo.json")
results = JSON.parse(server_info_json)

parsed_results = results['rows'].map do |res|
  ohai_attrs = res['automatic']

  {
    :name       => ohai_attrs['fqdn'],
    :os         => ohai_attrs['platform'],
    :os_version => ohai_attrs['platform_version'],
    :virtual    => true
  }
end

parsed_results.each do |srv|
  puts "   Name: #{srv[:name]}"
  puts "     OS: #{srv[:os]}"
  puts "Version: #{srv[:os_version]}"
  puts "Virtual: #{srv[:virtual]}"
  puts "\n"
end

