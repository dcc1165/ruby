#!/opt/webmd-ruby/embedded/bin/ruby
require "xmlrpc/client"

satellite_url = "http://spacewalk.portal.webmd.com/rpc/api"
satellite_login = ""
satellite_password = ""
new_base_channel = "centos-x86_64-server-6"
host_desc = "swsat06d-ops-08.portal.webmd.com"


@client = XMLRPC::Client.new2(satellite_url)

@key = @client.call('auth.login', satellite_login, satellite_password)
File.foreach('webopshostlist1.txt') { |host_desc|
    list = @client.call('system.search.nameAndDescription',@key,host_desc)
    p list.count
    for hostname in  list do
        result1 = @client.call('system.setBaseChannel', @key,hostname["id"],new_base_channel)
        if result1
           p "Successfully associated new base channel " + new_base_channel + " to the host " + hostname["name"]
           result2 = @client.call('system.setChildChannels', @key,hostname["id"],['centos-x86_64-server-6-platform-shared','centos-x86_64-server-6-epel','centos-x86_64-server-6-team-webops','centos-x86_64-server-6-u5-os','centos-x86_64-server-6-u5-updates','centos-x86_64-server-6-u5-extras'])
           if result2
              p "Successfully associated new child channels"
           end
        end
    end
}

@client.call('auth.logout', @key)
